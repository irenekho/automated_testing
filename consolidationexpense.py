from selenium import webdriver
from jurnal_navigations import Navigations
from expense_helper import ExpenseHelper
from selenium.webdriver.chrome.options import Options

chromeOptions = Options()
chromeOptions.add_argument("--kiosk")
driver = webdriver.Chrome(chrome_options=chromeOptions)
jn = Navigations(driver)
eh = ExpenseHelper(driver)

jn.open_sandbox()
jn.login_aim()
# n.activate_automation_company()

# Expense 5-50000
jn.case_name('expense 5-50000')
eh.new_expense()
eh.select_payfrom('1-10001')
eh.select_beneficiary('vendor 1')
eh.set_transaction_date('01/01/2018')
eh.set_payment_method('cash')
jn.scroll_page()
eh.add_first_account('5-50000', '', '15000')
eh.add_second_account('5-50100', '', '20000')
eh.insert_witholding_amount('1', '%', '1-10001')
eh.create_expense()
jn.check_journal_entry()

# Expense 5-50200
jn.case_name('expense 5-50400')
eh.new_expense()
eh.select_payfrom('1-10002')
eh.select_beneficiary('vendor 1')
eh.set_transaction_date('01/01/2018')
eh.set_payment_method('cash')
jn.scroll_page()
eh.add_first_account('5-50200', '', '15000')
eh.add_second_account('5-50300', '', '20000')
eh.insert_witholding_amount('5', '%', '1-10001')
eh.create_expense()
jn.check_journal_entry()

# Expense 5-50400 with tax
jn.case_name('expense 5-50400 with tax')
eh.new_expense()
eh.select_payfrom('1-10001')
eh.select_beneficiary('vendor 1')
eh.set_transaction_date('01/01/2018')
eh.set_payment_method('cash')
jn.scroll_page()
eh.add_first_account('5-50400', 'PPN', '15000')
eh.add_second_account('5-50500', 'PPN', '20000')
eh.insert_witholding_amount('5', '%', '1-10001')
eh.create_expense()
jn.check_journal_entry()

# Expense 6-60100 with tax
jn.case_name('expense 6-60001 with tax')
eh.new_expense()
eh.select_payfrom('1-10001')
eh.select_beneficiary('vendor 1')
eh.set_transaction_date('01/01/2018')
eh.set_payment_method('cash')
jn.scroll_page()
eh.add_first_account('6-60101', 'PPN', '15000')
eh.add_second_account('6-60102', 'PPN', '20000')
eh.insert_witholding_amount('5', '%', '1-10001')
eh.create_expense()
jn.check_journal_entry()

# Expense 6-60000 with tax
jn.case_name('expense 6-60000 with tax')
eh.new_expense()
eh.select_payfrom('1-10001')
eh.select_beneficiary('vendor 1')
eh.set_transaction_date('01/01/2018')
eh.set_payment_method('cash')
jn.scroll_page()
eh.add_first_account('6-60001', 'PPN', '15000')
eh.add_second_account('6-60002', 'PPN', '20000')
eh.insert_witholding_amount('5', '%', '1-10001')
eh.create_expense()
jn.check_journal_entry()

jn.end_test_case()
jn.close_browser()