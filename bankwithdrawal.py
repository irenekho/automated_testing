from selenium import webdriver
from jurnal_navigations import Navigations
from withdrawal_helper import BankWithdrawalHelper
from selenium.webdriver.chrome.options import Options

chromeOptions = Options()
chromeOptions.add_argument("--kiosk")
driver = webdriver.Chrome()
#driver = webdriver.Chrome(chrome_options=chromeOptions)
n = Navigations(driver)
bw = BankWithdrawalHelper(driver)

n.open_newblueranger()
n.login_irene()

n.open_bankwithdrawal_index()

bw.create_new_bankwithdrawal()
bw.select_payfrom('cash')
bw.select_payee('irene')
bw.set_transaction_date('01/01/2018')
bw.select_tags('tag1')
bw.tax_inclusive_toggle()
bw.scroll_page()
bw.select_account('cash')
bw.add_description('automated testing')
bw.select_tax('ppn')
bw.add_amount('100000')
bw.scroll_page()
bw.add_memo('aim bulat')
bw.click_create_button()

n.view_journal_entry()
n.check_journal_entry()
n.close_browser()