from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import NoSuchElementException, TimeoutException
import time


class SalesHelper(object):

    # static variables
    finish_loading = ec.invisibility_of_element_located(
        (By.CSS_SELECTOR, 'input.select2-input.select2-focused.select2-active'))

    searched_item = 'div.select2-result-label:nth-child(1)'

    def __init__(self, driver):
        self.driver = driver

    def view_sales_invoice_index(self):
        self.driver.find_element_by_id('tab-header-1').click()

    def view_sales_delivery_index(self):
        self.driver.find_element_by_id('tab-header-4').click()

    def view_sales_order_index(self):
        self.driver.find_element_by_id('tab-header-2').click()

    def view_sales_quote_index(self):
        self.driver.find_element_by_id('tab-header-3').click()

    def new_sales_invoice(self):
        try:
            WebDriverWait(self.driver, 10).until(
                ec.invisibility_of_element_located((By.CSS_SELECTOR, 'div.dropdown-backdrop'))
            )
        finally:
            self.driver.find_element_by_css_selector('a[href="/invoices/new"]').click()
            print('creating new sales invoice')

    def new_sales_order(self):
        try:
            WebDriverWait(self.driver, 10).until(
                ec.invisibility_of_element_located((By.CSS_SELECTOR, 'div.dropdown-backdrop'))
            )
        finally:
            self.driver.find_element_by_css_selector('a[href="/invoices/new"]').click()
            self.driver.find_element_by_id('s2id_selected_type').click()
            sales_order = self.driver.find_element_by_id('select2-result-label-26')
            action = webdriver.ActionChains(self.driver)
            action.move_to_element(sales_order)
            action.click(sales_order)
            action.perform()
            print('creating new sales order')

    def new_sales_quote(self):
        try:
            WebDriverWait(self.driver, 10).until(
                ec.invisibility_of_element_located((By.CSS_SELECTOR, 'div.dropdown-backdrop'))
            )
        finally:
            self.driver.find_element_by_css_selector('a[href="/invoices/new"]').click()
            self.driver.find_element_by_id('s2id_selected_type').click()
            sales_quote = self.driver.find_element_by_id('select2-result-label-27')
            action = webdriver.ActionChains(self.driver)
            action.move_to_element(sales_quote)
            action.click(sales_quote)
            action.perform()
            print('creating new sales quote')

    def is_using_mc(self):
        try:
            self.driver.find_element_by_id('s2id_transaction_currency_list_id')
            return True
        except NoSuchElementException:
            return False

    def select_customer(self, customer):
        self.is_using_mc()
        self.driver.find_element_by_id('s2id_transaction_person_id').click()
        try:
            WebDriverWait(self.driver, 10).until(self.finish_loading)
        finally:
            if self.is_using_mc() is True:
                customer_field = 's2id_autogen14_search'
            else:
                customer_field = 's2id_autogen13_search'
            self.driver.find_element_by_id(customer_field).send_keys(customer)
            WebDriverWait(self.driver, 10).until(self.finish_loading)
            self.driver.find_element_by_css_selector(self.searched_item).click()

    def set_transaction_date(self, date):
        date_field = self.driver.find_element_by_id('transaction_transaction_date')
        date_field.click()
        time.sleep(0.5)
        date_field.clear()
        date_field.send_keys(date)
        self.driver.find_element_by_css_selector('td.active.day').click()

    def add_tags(self, tags):
        self.is_using_mc()
        for tag in tags:
            self.driver.find_element_by_id('s2id_transaction_tag_ids').click()
            if self.is_using_mc() is True:
                select2_tag = 's2id_autogen13'
            else:
                select2_tag = 's2id_autogen12'
            WebDriverWait(self.driver, 3).until(self.finish_loading)
            self.driver.find_element_by_id(select2_tag).send_keys(tag)
            WebDriverWait(self.driver, 3).until(self.finish_loading)
            self.driver.find_element_by_id(select2_tag).send_keys(Keys.RETURN)
            self.driver.find_element_by_css_selector('label.control-label').click()

    def insert_witholding_amount(self, amount, unit, account_number):
        self.is_using_mc()
        self.driver.find_element_by_id('witholding_toggle').click()
        WebDriverWait(self.driver, 10).until(self.finish_loading)
        if unit != '%':
            self.driver.find_element_by_css_selector(
                'button.btn.btn-default.btn-switch.btn-toggle-percent-values.toggle-currency-symbol:nth-child(2)'
            ).click()
        else:
            pass
        witholding_value = self.driver.find_element_by_name('transaction[witholding_value]')
        witholding_value.clear()
        witholding_value.send_keys(amount)
        self.driver.find_element_by_css_selector('div.copyright.col-lg-8.col-md-8.col-sm-8').click()
        self.driver.find_element_by_id('s2id_witholding_account').click()
        try:
            WebDriverWait(self.driver, 10).until(self.finish_loading)
        finally:
            if self.is_using_mc() is True:
                select_witholding_account = 's2id_autogen24_search'
            else:
                select_witholding_account = 's2id_autogen23_search'
            self.driver.find_element_by_id(select_witholding_account).send_keys(account_number)
            WebDriverWait(self.driver, 10).until(self.finish_loading)
            self.driver.find_element_by_css_selector(self.searched_item).click()

    def add_deposit(self, amount, account_number):
        self.is_using_mc()
        self.driver.find_element_by_id('transaction_deposit').send_keys(amount)
        self.driver.find_element_by_css_selector('div.copyright.col-lg-8.col-md-8.col-sm-8').click()
        self.driver.find_element_by_id('s2id_invoice_deposit_to').click()
        try:
            WebDriverWait(self.driver, 10).until(self.finish_loading)
        finally:
            if self.is_using_mc() is True:
                select_deposit_account = 's2id_autogen23_search'
            else:
                select_deposit_account = 's2id_autogen22_search'
            self.driver.find_element_by_id(select_deposit_account).send_keys(account_number)
            WebDriverWait(self.driver, 10).until(self.finish_loading)
            self.driver.find_element_by_css_selector(self.searched_item).click()

    def create_sales(self):
        self.driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        self.driver.find_element_by_css_selector('div.copyright.col-lg-8.col-md-8.col-sm-8').click()
        time.sleep(0.5)
        self.driver.find_element_by_id('create_button').click()

    def add_receive_payment(self):
        self.driver.find_element_by_css_selector('div.btn-group.dropup.print-preview:nth-child(2)').click()
        self.driver.find_element_by_xpath('//li[contains(text(), "Receive Payment")]').click()

    def send_delivery(self):
        self.driver.find_element_by_css_selector('span.fa.fa-bars.fa-1x').click()
        send_delivery = self.driver.find_element_by_link_text('Create Delivery')
        action = webdriver.ActionChains(self.driver)
        action.move_to_element(send_delivery)
        action.click(send_delivery)
        action.perform()
        print('creating sales delivery')

    def add_shipping_fee(self, fee):
        shipping_fee_field = self.driver.find_element_by_id('transaction_shipping_price')
        shipping_fee_field.click()
        time.sleep(0.5)
        shipping_fee_field.clear()
        shipping_fee_field.send_keys(fee)

    def create_sales_delivery(self):
        self.driver.find_element_by_css_selector('div.copyright.col-lg-8.col-md-8.col-sm-8').click()
        time.sleep(0.5)
        self.driver.find_element_by_id('update_invoice').click()

    def create_sales_from_delivery(self):
        self.driver.find_element_by_css_selector('div.copyright.col-lg-8.col-md-8.col-sm-8').click()
        time.sleep(0.5)
        self.driver.find_element_by_link_text('Create Invoice').click()
        print('creating sales invoice from delivery')

    def add_first_product(self, product_name, qty, unit_price='', discount='', tax_line=''):
        self.is_using_mc()
        # isi product_name
        self.driver.find_element_by_xpath('//span[contains(text(), "Select product")]').click()
        try:
            WebDriverWait(self.driver, 10).until(self.finish_loading)
        finally:
            if self.is_using_mc() is True:
                select_product = 's2id_autogen15_search'
            else:
                select_product = 's2id_autogen14_search'
            product_input = self.driver.find_element_by_id(select_product)
            product_input.clear()
            product_input.send_keys(product_name)
            WebDriverWait(self.driver, 10).until(self.finish_loading)
            first_product = self.driver.find_element_by_css_selector(self.searched_item)
            action = webdriver.ActionChains(self.driver)
            action.move_to_element(first_product)
            action.click(first_product)
            action.perform()

        # isi qty
        qty_field = self.driver.find_element_by_id('transaction_transaction_lines_attributes_0_quantity')
        qty_field.click()
        time.sleep(0.5)
        qty_field.clear()
        qty_field.send_keys(qty)
        self.driver.find_element_by_css_selector('th.qty-line').click()

        # isi unit_price
        if unit_price != '':
            price_field = self.driver.find_element_by_id('transaction_transaction_lines_attributes_0_rate')
            price_field.click()
            time.sleep(0.5)
            price_field.clear()
            price_field.send_keys(unit_price)
            self.driver.find_element_by_css_selector('th.price-line').click()
            inputted_price = self.driver.find_element_by_id(
                'transaction_transaction_lines_attributes_0_rate').get_attribute('data-value')
            count = 0
            while float(inputted_price) != float(unit_price) and count < 2:
                input_price.click()
                time.sleep(1)
                input_price.clear()
                input_price.send_keys(unit_price)
                self.driver.find_element_by_css_selector('th.price-line').click()
                count += 1
        else:
            pass

        # isi discount
        if discount != '':
            discount_field = self.driver.find_element_by_id('transaction_transaction_lines_attributes_0_discount')
            discount_field.click()
            time.sleep(1)
            discount_field.clear()
            discount_field.send_keys(discount)
            self.driver.find_element_by_css_selector('th.discount-line').click()
        else:
            pass

        # isi tax
        if tax_line != '':
            self.driver.find_element_by_id('s2id_transaction_transaction_lines_attributes_0_line_tax_id').click()
            WebDriverWait(self.driver, 10).until(self.finish_loading)
            tax_line_field = self.driver.find_element_by_id('s2id_autogen17_search')
            tax_line_field.click()
            time.sleep(1)
            tax_line_field.clear()
            tax_line_field.send_keys(tax_line)
            WebDriverWait(self.driver, 10).until(self.finish_loading)
            first_tax = self.driver.find_element_by_css_selector(self.searched_item)
            action.move_to_element(first_tax)
            action.click(first_tax)
            action.perform()
        else:
            pass

    def add_second_product(self, product_name, qty, unit_price='', discount='', tax_line=''):

        # isi product_name
        self.driver.find_element_by_xpath('//span[contains(text(), "Select product")]').click()
        time.sleep(1)
        try:
            WebDriverWait(self.driver, 10).until(self.finish_loading)
        finally:
            product_input = self.driver.find_element_by_id('s2id_autogen16_search')
            product_input.clear()
            product_input.send_keys(product_name)
            WebDriverWait(self.driver, 10).until(self.finish_loading)
            second_product = self.driver.find_element_by_css_selector(self.searched_item)
            action = webdriver.ActionChains(self.driver)
            action.move_to_element(second_product)
            action.click(second_product)
            action.perform()

        # isi qty
        qty_field = self.driver.find_element_by_id('transaction_transaction_lines_attributes_1_quantity')
        qty_field.click()
        time.sleep(0.5)
        qty_field.clear()
        qty_field.send_keys(qty)
        self.driver.find_element_by_css_selector('th.qty-line').click()

        # isi unit_price
        if unit_price != '':
            price_field = self.driver.find_element_by_id('transaction_transaction_lines_attributes_1_rate')
            price_field.click()
            time.sleep(0.5)
            price_field.clear()
            price_field.send_keys(unit_price)
            self.driver.find_element_by_css_selector('th.price-line').click()
            inputted_price = self.driver.find_element_by_id(
                'transaction_transaction_lines_attributes_1_rate').get_attribute('data-value')
            count = 0
            while float(inputted_price) != float(unit_price) and count < 2:
                input_price.click()
                time.sleep(1)
                input_price.clear()
                input_price.send_keys(unit_price)
                self.driver.find_element_by_css_selector('th.price-line').click()
                count += 1
        else:
            pass

        # isi discount
        if discount != '':
            discount_field = self.driver.find_element_by_id('transaction_transaction_lines_attributes_1_discount')
            discount_field.click()
            time.sleep(1)
            discount_field.clear()
            discount_field.send_keys(discount)
            self.driver.find_element_by_css_selector('th.discount-line').click()
        else:
            pass

        # isi tax
        if tax_line != '':
            self.driver.find_element_by_id('s2id_transaction_transaction_lines_attributes_1_line_tax_id').click()
            WebDriverWait(self.driver, 10).until(self.finish_loading)
            tax_line_field = self.driver.find_element_by_id('s2id_autogen18_search')
            tax_line_field.click()
            time.sleep(1)
            tax_line_field.clear()
            tax_line_field.send_keys(tax_line)
            WebDriverWait(self.driver, 10).until(self.finish_loading)
            second_tax = self.driver.find_element_by_css_selector(self.searched_item)
            action.move_to_element(second_tax)
            action.click(second_tax)
            action.perform()
        else:
            pass

    def new_sales_return(self):
        self.driver.find_element_by_css_selector('div.copyright.col-lg-8.col-md-8.col-sm-8').click()
        self.driver.find_element_by_css_selector('span.fa.fa-bars.fa-1x').click()
        time.sleep(0.5)
        self.driver.find_element_by_link_text('Sales Return').click()
        print('creating sales return')

    def set_sales_return_date(self, date):
        date_field = self.driver.find_element_by_id('sales_return_transaction_date')
        date_field.click()
        time.sleep(0.5)
        date_field.clear()
        date_field.send_keys(date)
        self.driver.find_element_by_css_selector('td.active.day').click()

    def sales_return_first_product_qty(self, qty):
        first_qty = self.driver.find_element_by_id('sales_return_transaction_return_lines_attributes_0_return_quantity')
        first_qty.click()
        time.sleep(0.5)
        first_qty.clear()
        first_qty.send_keys(qty)
        self.driver.find_element_by_css_selector('th.col-md-1.col-lg-1').click()

    def sales_return_second_product_qty(self, qty):
        second_qty = self.driver.find_element_by_id(
            'sales_return_transaction_return_lines_attributes_1_return_quantity')
        second_qty.click()
        time.sleep(0.5)
        second_qty.clear()
        second_qty.send_keys(qty)
        self.driver.find_element_by_css_selector('th.col-md-1.col-lg-1').click()

    def create_sales_return(self):
        self.driver.find_element_by_css_selector('div.copyright.col-lg-8.col-md-8.col-sm-8').click()
        time.sleep(0.5)
        self.driver.find_element_by_id('create_sales_return').click()

    def use_mc(self, currency, rate=''):
        self.driver.find_element_by_id('s2id_transaction_currency_list_id').click()
        input_mc = self.driver.find_element_by_id('s2id_autogen3_search')
        input_mc.click()
        input_mc.send_keys(currency)
        self.driver.find_element_by_css_selector('span.select2-match').click()
        if rate != '':
            time.sleep(1)
            self.driver.find_element_by_id('edit_currency_href').click()
            WebDriverWait(self.driver, 10).until(ec.presence_of_element_located((By.CSS_SELECTOR, 'div.modal-dialog')))
            custom_currency = self.driver.find_element_by_id('custom_currency_field')
            custom_currency.click()
            custom_currency.send_keys(rate)
            self.driver.find_element_by_css_selector('button.btn.btn-success.edit_multi_currency').click()
        else:
            pass

    def add_message(self, message):
        addmessage = self.driver.find_element_by_id('transaction_message')
        addmessage.click()
        addmessage.send_keys(message)

    def add_memo(self, memo):
        addmemo = self.driver.find_element_by_id('transaction_memo')
        addmemo.click()
        addmemo.send_keys(memo)


# import masih belum selesai, bisa dipindah ke module baru khusus import
#    def import_sales_(self, path):
#        self.driver.find_element_by_css_selector('button.btn.btn-default.import-button.import-sales-index-button').click()
#        self.driver.find_element_by_css_selector('button[data-target="#import_modal"]').click()
#        self.driver.find_element_by_css_selector('button.btn.btn-action-import2.dropdown-toggle.no-left-radius').click()
#        self.driver.find_element_by_css_selector('a.btn-select-invoice-template.import-upload-template-sales-invoice.dz-clickable:nth-child(1)').click()
