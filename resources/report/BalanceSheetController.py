from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.common.by import By
from enum import Enum
from report.ReportsHelper import ReportsHelper


class BalanceSheetDict(dict):

    def __getattr__(self, item):
        if item in self:
            return self[item]
        raise AttributeError(item)

    COMPARE_PERIOD = {"none": 'li[value="none"]',
                      "weekly": 'li[value="weekly"]',
                      "monthly": 'li[value="monthly"]',
                      "quarterly": 'li[value="quarterly"]',
                      "semi yearly": 'li[value="semi_yearly"]',
                      "yearly": 'li[value="yearly"]'}


class BalanceSheetLocators(Enum):
    COMPARE_DATA = 'compare-data'
    PERIOD_ORDER_GROUP = 'period-order-group'
    ASC = 'input[value="asc"]'
    DESC = 'input[value="desc"]'


class BalanceSheetController(ReportsHelper):

    def __init__(self, driver):
        ReportsHelper.__init__(self, driver)

    def set_as_of(self, date, is_popup='no'):
        if is_popup == 'no':
            super(BalanceSheetController, self).set_date_filter(date, date_type='end', at_popup='no')
        else:
            super(BalanceSheetController, self).set_date_filter(date, date_type='end', at_popup='yes')

    def period_comparison(self, period):
        period_comparison = self.driver.find_element_by_id(BalanceSheetLocators.COMPARE_DATA.value)
        super(BalanceSheetController, self).select_item(period_comparison)
        super(BalanceSheetController, self).wait().until(
            ec.element_to_be_clickable((By.CSS_SELECTOR, BalanceSheetDict.COMPARE_PERIOD[period])))
        selected_period = self.driver.find_element_by_css_selector(BalanceSheetDict.COMPARE_PERIOD[period])
        super(BalanceSheetController, self).select_item(selected_period)
        print('Using ' + period + ' comparison')

    def order_period(self, order='descending'):  # order is asc and desc
        if order != 'descending':
            period_order = BalanceSheetLocators.ASC.value
        else:
            period_order = BalanceSheetLocators.DESC.value
        period_order_group = self.driver.find_element_by_id(BalanceSheetLocators.PERIOD_ORDER_GROUP.value)
        order_tickbox = period_order_group.find_element_by_css_selector(period_order)
        super(BalanceSheetController, self).select_item(order_tickbox)
