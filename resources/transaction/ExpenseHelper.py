from common.GeneralFunctions import GeneralFunctions
from common.TransactionLocators import TransactionLocators


class ExpenseHelper(GeneralFunctions):

    def __init__(self, driver):
        GeneralFunctions.__init__(self, driver)
        self.driver = driver

    def select_payfrom(self, account):
        super(ExpenseHelper, self).select2_search_and_select(TransactionLocators.PAYFROM.value, account)

    def tick_pay_later(self):
        pay_later = self.driver.find_element_by_id(TransactionLocators.PAY_LATER.value)
        super(ExpenseHelper, self).select_item(pay_later)

    def set_payment_method(self, payment_method):
        super(ExpenseHelper, self).select2_search_and_select(TransactionLocators.PAYMENT_METHOD.value, payment_method)

    def set_transaction_amount(self, line, amount):
        super(ExpenseHelper, self).set_account_amount(line, amount, amount_type='debit')
