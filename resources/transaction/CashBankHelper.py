from enum import Enum
from common.GeneralFunctions import GeneralFunctions


class CashBankLocators(Enum):

    TRANSFER_FROM = 's2id_transaction_refund_from_id'
    DEPOSIT_TO = 's2id_transaction_deposit_to_id'
    TRANSFER_AMOUNT = 'transaction_transfer_amount'
    ACCOUNT_LINE = ['s2id_transaction_transaction_account_lines_attributes_0_account_id',
                    's2id_transaction_transaction_account_lines_attributes_1_account_id']
    RECEIVED_AMOUNT = ['transaction_transaction_account_lines_attributes_0_credit',
                       'transaction_transaction_account_lines_attributes_1_credit']
    PAID_AMOUNT = ['transaction_transaction_account_lines_attributes_0_debit',
                   'transaction_transaction_account_lines_attributes_1_debit']


class CashBankHelper(GeneralFunctions):

    def __init__(self, driver):
        GeneralFunctions.__init__(self, driver)
        self.driver = driver

    def transfer_from(self, account):
        super(CashBankHelper, self).select2_search_and_select(CashBankLocators.TRANSFER_FROM.value, account)

    def deposit_to(self, account):
        super(CashBankHelper, self).select2_search_and_select(CashBankLocators.DEPOSIT_TO.value, account)

    def set_transfer_amount(self, amount):
        transfer_amount_field = self.driver.find_element_by_id(CashBankLocators.TRANSFER_AMOUNT.value)
        super(CashBankHelper, self).input_item(transfer_amount_field, amount)

    def add_account(self, line, account):
        if line == 1:
            account_id = CashBankLocators.ACCOUNT_LINE.value[0]
        else:
            account_id = CashBankLocators.ACCOUNT_LINE.value[1]
        super(CashBankHelper, self).select2_search_and_select(account_id, account)

    def set_amount(self, line, amount, amount_type):
        if line == 1:
            if amount_type == 'receive':
                amount_id = CashBankLocators.RECEIVED_AMOUNT.value[0]
            else:
                amount_id = CashBankLocators.PAID_AMOUNT.value[0]
        else:
            if amount_type == 'receive':
                amount_id = CashBankLocators.RECEIVED_AMOUNT.value[1]
            else:
                amount_id = CashBankLocators.PAID_AMOUNT.value[1]
        amount_field = self.driver.find_element_by_id(amount_id)
        super(CashBankHelper, self).input_item(amount_field, amount)

    def add_account_line(self, line, account, tax, amount, transaction_type):
        self.add_account(line, account)
        super(CashBankHelper, self).add_tax(line, tax)
        if transaction_type == 'deposit':
            self.set_amount(line, amount, amount_type='receive')
        else:
            self.set_amount(line, amount, amount_type='pay')
