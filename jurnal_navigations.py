from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import *
from datetime import datetime


class Navigations(object):

    def __init__(self, driver):
        self.driver = driver

    # str(datetime.now()) buat ngeprint jam waktu test case dimulai, biar bisa track berapa menit jalanin test case

    def open_ppe(self):
        print('\n' + 'test cases started at ' + str(datetime.now()) + '\n')
        self.driver.get('http://ppe-jurnal-1.ap-southeast-1.elasticbeanstalk.com')
        self.driver.maximize_window()
        print('Jurnal ppe is opened')

    def open_blueranger(self):
        print('\n' + 'test cases started at ' + str(datetime.now()) + '\n')
        self.driver.get('http://bluerangers1.ap-southeast-1.elasticbeanstalk.com/')
        self.driver.maximize_window()
        print('Blueranger is opened')

    def open_newblueranger(self):
        print('\n' + 'test cases started at ' + str(datetime.now()) + '\n')
        self.driver.get('https://blue.cd.jurnal.id/')
        self.driver.maximize_window()
        print('New Blueranger is opened')

    def open_jurnal_ap(self):
        print('\n' + 'test cases started at ' + str(datetime.now()) + '\n')
        self.driver.get('http://jurnal.ap-southeast-1.elasticbeanstalk.com/')
        self.driver.maximize_window()
        print('Jurnal ap is opened')

    def open_sandbox(self):
        self.driver.get('http://sandbox.jurnal.id/')
        self.driver.maximize_window()
        print('Sandbox is opened')

    def login_gara(self):
        email_login = self.driver.find_element_by_name('user[email]')
        email_login.clear()
        email_login.send_keys('gara.handhito@jurnal.id')
        password_login = self.driver.find_element_by_name('user[password]')
        password_login.clear()
        password_login.send_keys('64r$h4ndh1t)')
        password_login.send_keys(Keys.RETURN)
        assert 'Dashboard' or 'Dasbor' in driver.title
        print('Logged in as Gara')

    def login_aim(self):
        email_login = self.driver.find_element_by_name('user[email]')
        email_login.clear()
        email_login.send_keys('abbad.imad@jurnal.id')
        password_login = self.driver.find_element_by_name('user[password]')
        password_login.clear()
        password_login.send_keys('jurnal123')
        password_login.send_keys(Keys.RETURN)
        assert 'Dashboard' or 'Dasbor' in driver.title
        print('Logged in as Aim')

    def login_irene(self):    
        email_login = self.driver.find_element_by_name('user[email]')
        email_login.clear()
        email_login.send_keys('irene@jurnal.id')
        password_login = self.driver.find_element_by_name('user[password]')
        password_login.clear()
        password_login.send_keys('irene123')
        password_login.send_keys(Keys.RETURN)
        assert 'Dashboard' or 'Dasbor' in driver.title
        print('Logged in as Irene')

    # ini udah gak perlu dipake lagi, nama company yg diaktifin tulis aja di parameter function activate_company()

    # def activate_automation_company(self):
    #     self.driver.find_element_by_css_selector('div.nav-user-text').click()
    #     self.driver.find_element_by_link_text('Company List').click()
    #
    #     automation_company = self.driver.find_element_by_link_text('Automation')
    #     self.driver.execute_script("arguments[0].scrollIntoView();", automation_company)
    #     automation_company.click()
    #
    #     print('Automation company is active')

    def activate_company(self, company_name):
        self.driver.find_element_by_css_selector('div.nav-user-text').click()
        self.driver.find_element_by_link_text('Company List').click()
        company = self.driver.find_element_by_link_text(company_name)
        self.driver.execute_script("arguments[0].scrollIntoView();", company)
        company.click()
        print(company_name + ' company is active \n')

    def open_reports_index(self):
        self.driver.find_element_by_id('vnav-reports-link').click()
        print('Reports index is opened')

    def open_cashbank_index(self):
        self.driver.find_element_by_id('vnav-cashbank-link').click()
        print('Cash & Bank index is opened')

    def open_sales_index(self):
        self.driver.find_element_by_id('vnav-sales-link').click()
        print('Sales index is opened')

    def open_purchases_index(self):
        self.driver.find_element_by_id('vnav-purchases-link').click()
        print('Purchases index is opened')

    def open_expenses_index(self):
        self.driver.find_element_by_id('vnav-expenses-link').click()
        print('Expenses index is opened')

    def open_bankdeposit_index(self):
        self.driver.find_element_by_id('vnav-cashbank-link').click()
        print('Bank Deposit index is opened')

    def open_banktransfer_index(self):
        self.driver.find_element_by_id('vnav-cashbank-link').click()
        print('Bank Transfer index is opened')

    def open_customers_index(self):
        self.driver.find_element_by_id('vnav-customers-link').click()
        print('Customers index is opened')

    def open_vendors_index(self):
        self.driver.find_element_by_id('vnav-vendors-link').click()
        print('Vendors index is opened')

    def open_products_index(self):
        self.driver.find_element_by_id('vnav-inventory-link').click()
        print('Products index is opened')

    def open_assets_index(self):
        self.driver.find_element_by_id('vnav-assetmanagement-link').click()
        print('Assets index is opened')

    def open_accounts_index(self):
        self.driver.find_element_by_id('vnav-chartofaccounts-link').click()
        print('Chart of Accounts is opened')

    def open_other_lists(self):
        self.driver.find_element_by_id('vnav-otherlists-link').click()
        print('Other lists is opened')

    def open_add_ons(self):
        self.driver.find_element_by_id('vnav-add-ons-link').click()
        print('Add-ons index is opened')

    def open_settings(self):
        self.driver.find_element_by_id('vnav-settings-link').click()
        print('Settings is opened')

    def logout_user(self):
        self.driver.find_element_by_id('vnav-logout-link').click()
        print('Gara has logged out')

    def close_browser(self):
        self.driver.quit()
        print('Browser has been closed')

    def tax_inclusive_toggle(self):
        self.driver.find_element_by_class_name('tax-inclusive-toogle').click()

    def scroll_page(self):
        element = self.driver.find_element_by_id('create_button')
        actions = ActionChains(self.driver)
        actions.move_to_element(element).perform()

    def check_journal_entry(self):
        refresh = self.driver.refresh()
        self.driver.find_element_by_css_selector('a.small_journal_link.get-ajax-account-transaction').click()
        WebDriverWait(self.driver, 10, poll_frequency=1, ignored_exceptions=[ElementNotVisibleException, ElementNotSelectableException]).until(
            ec.invisibility_of_element_located((By.CSS_SELECTOR, 'div.blinkdot')))
        debit = self.driver.find_element_by_css_selector('td.grand-total:nth-child(3)').get_attribute('innerHTML')
        credit = self.driver.find_element_by_css_selector('td.grand-total:nth-child(4)').get_attribute('innerHTML')
        assert debit == credit
        index = 0
        while ((debit != credit) or (debit == '0,00' and credit == '0,00')) and index < 2:
            self.driver.refresh()
            self.driver.find_element_by_css_selector('a.small_journal_link.get-ajax-account-transaction').click()
            WebDriverWait(self.driver, 10, poll_frequency=1, ignored_exceptions=[ElementNotVisibleException, ElementNotSelectableException]).until(
                ec.presence_of_all_elements_located((By.CSS_SELECTOR, 'div.modal-body')))
            assert debit == credit
            index += 1
        if debit == credit and debit != '0,00' and credit != '0,00':
            print('debit is ' + debit + '\n' + 'credit is ' + credit)
            print('journal entry is balanced \n')
            self.driver.refresh()
        elif (debit == '0,00') or (credit == '0,00'):
            print('debit or credit is 0, please check sidekiq \n')
            self.driver.refresh()
        else:
            print('debit is ' + debit + '\n' + 'credit is ' + credit)
            print('journal entry is not balanced \n')
            self.driver.refresh()

    def view_gl_journal_entry(self):
        try:
            self.driver.refresh()
            self.driver.find_element_by_css_selector('a.small_journal_link.get-ajax-account-transaction').click()
            WebDriverWait(self.driver, 3).until(
                ec.visibility_of_element_located((By.CSS_SELECTOR, 'tr.gl_account_lines:nth-child(2)'))
                )
        except (TimeoutException, TimeoutError, NoSuchElementException):
            pass

    def check_gl_journal_entry(self):
        self.view_gl_journal_entry()
        try:
            debit = self.driver.find_element_by_css_selector('td.gl_total_debit').get_attribute('innerHTML')
            credit = self.driver.find_element_by_css_selector('td.gl_total_credit').get_attribute('innerHTML')
            index = 0
            while (debit != credit or (debit == '0,00' or credit == '0,00')) and index < 2:
                self.view_gl_journal_entry()
                assert debit == credit
                index += 1
            if debit == credit and debit != '0,00' and credit != '0,00':
                print('debit is ' + debit + '\n' + 'credit is ' + credit)
                print('gl journal entry is balanced \n')
                self.driver.refresh()
            elif debit == '0,00' or credit == '0,00':
                print('debit or credit is 0, please check journal_fact')
                print(self.driver.current_url + '\n')
                self.driver.refresh()
            else:
                print('debit is ' + debit + '\n' + 'credit is ' + credit)
                print('gl journal entry is not balanced')
                print(self.driver.current_url + '\n')
                self.driver.refresh()
        except NoSuchElementException:
            print('failed to create transaction' + '\n')
            pass

    # ini buat ngeprint nama casenya di log
    @staticmethod
    def case_name(case):
        print(case)

    # ini buat ngeprint jam dan waktu test case selesai, biar bisa ditrack kapan mulai dan selesai
    @staticmethod
    def end_test_case():
        print('test cases finished at ' + str(datetime.now()))

    def check_error500(self):
        try:
            self.driver.find_element_by_css_selector('div.col-xs-12 error500-icon')
            self.driver.driver.get_screenshot_as_file('\jurnal\Pictures\Screenshots\error500')
            print('error 500 found')
            print(self.driver.current_url + '\n')
            self.driver.find_element_by_css_selector('a.btn.btn-404').click()
        except NoSuchElementException:
            pass

    def scroll_page(self):
        element = self.driver.find_element_by_id('create_button')
        actions = ActionChains(self.driver)
        actions.move_to_element(element).perform()